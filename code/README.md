# README #

Projeto de Graduação de Luiz Vitor França Lima: _SAP – Sistema de Apoio ao Professor_.

### Resumo ###

No meio acadêmico, é comum os professores que ministram alguma disciplina aplicarem trabalhos para os alunos que estão matriculados. Neste projeto foi desenvolvida uma apli- cação Web Java para ser utilizada inicialmente pelos professores do DI/Ufes. Seu objetivo é automatizar alguns processos acadêmicos utilizando a informática para diminuir o trabalho manual do professor, assim como otimizar o seu tempo para que possa desenvolver outras atividades. Utilizando o sistema, o professor poderá cadastrar disciplinas, turmas, traba- lhos, importar os dados dos alunos matriculados, além de cadastrar também as orientações que ele realiza junto aos alunos, dentre outras funcionalidades. O desenvolvimento seguiu um processo de Engenharia de Software, utilizando métodos e técnicas de modelagem e desenvolvimento orientado a objetos, em particular o método FrameWeb para projeto de aplicações Web baseadas em frameworks.
