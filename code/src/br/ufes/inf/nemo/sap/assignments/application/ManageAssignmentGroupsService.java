package br.ufes.inf.nemo.sap.assignments.application;

import java.util.List;

import javax.ejb.Local;

import br.ufes.inf.nemo.sap.assignments.domain.*;
import br.ufes.inf.nemo.util.ejb3.application.CrudService;

/**
 * Local EJB interface for the component that implements the "Manage Assignment Groups" use case.
 * 
 * This use case consists of a CRUD for the class AssignmentGroup and uses the mini CRUD framework for EJB3.
 * 
 * @author Luiz Vitor Franca Lima
 */

@Local
public interface ManageAssignmentGroupsService extends CrudService<AssignmentGroup> {
	/************************************************************************************************** 
	 * Method used to return a list of all assignmentGroups.
	 * 
	 * @return 
	 * 		A list of all assignmentGroup objects.
	 ***************************************************************************************************/	
	public List<AssignmentGroup> getAssignmentGroups();
	
	/************************************************************************************************** 
	 * Method used to return a list of the assignments of a schoolRoom.
	 * 
	 * @param schoolRoom
	 * 		The exact schoolRoom of the assignments to be retrieved.
	 * 
	 * @return 
	 * 		A list of assignments objects.
	 ***************************************************************************************************/
	public List<AssignmentGroup> getAssignmentGroups(Assignment assignment);
}