% ==============================================================================
% TCC - Luiz Vitor França Lima
% Capítulo 2 - Referencial Teórico
% ==============================================================================
\chapter{Referencial Teórico}
\label{sec-referencial}

Nesta seção, iremos abordar os principais conceitos teóricos que foram utilizados para o desenvolvimento do sistema SAP.

%%% Início de seção. %%%
\section{Engenharia de Software}
\label{sec-referencial-engenharia-software}

O desenvolvimento de software tem crescido bastante nos últimos anos devido a sua grande importância na sociedade contemporânea. O crescimento do uso de computadores pessoais, nas organizações e nas diversas áreas do conhecimento humano gerou uma crescente demanda por soluções que realizam a automatização dos processos.

Pessoas que estão iniciando na área de desenvolvimento de software têm o costume de confundir desenvolvimento com programação, pois estes estão em fase de desenvolver suas habilidades no raciocínio lógico na resolução de pequenos problemas. Entretanto, ao se deparar com problemas mais complexos que requerem maior conhecimento e habilidades, uma abordagem centrada na programação não é a mais indicada.

A \textbf{Engenharia de Software} surgiu com o intuito de melhorar a qualidade dos softwares em geral e aumentar a produtividade no desenvolvimento de tais produtos, sendo responsável pelo estabelecimento de técnicas e práticas para o desenvolvimento de software cobrindo uma ampla área de aplicações e diferentes tipos de dispositivos, tais como sistemas de informação corporativos, sistemas e portais Web, aplicações em telefones celulares e tablets, entre outros~\cite{falboEngReq}.

Um processo de software, em uma abordagem de Engenharia de Software, envolve diversas atividades que podem ser classificadas quanto ao seu propósito em~\cite{falboEngReq}:

\begin{itemize}
	\item Atividades de Desenvolvimento (ou Técnicas): são as atividades diretamente relacionadas ao processo de desenvolvimento do software, ou seja, que contribuem diretamente para o desenvolvimento do produto de software a ser entregue ao cliente. São exemplos de atividades de desenvolvimento: levantamento e análise de requisitos, projeto e implementação;
	
	\item Atividades de Gerência: envolvem atividades relacionadas ao gerenciamento do projeto de maneira abrangente. Incluem, dentre outras: atividades de planejamento e acompanhamento gerencial do projeto (processo de Gerência de Projetos), tais como realização de estimativas, elaboração de cronogramas, análise dos riscos do projeto etc.; atividades relacionadas à gerência da evolução dos diversos artefatos produzidos nos projetos de software (processo de Gerência de Configuração); atividades relacionadas à gerência de ativos reutilizáveis de uma organização (processo de Gerência de Reutilização), etc;
	
	\item Atividades de Controle da Qualidade: são aquelas relacionadas com a avaliação da qualidade do produto em desenvolvimento e do processo de software utilizado. Incluem atividades de verificação, validação e garantia da qualidade.
\end{itemize}

As atividades de desenvolvimento formam a espinha dorsal do desenvolvimento e, de maneira geral, são realizadas segundo uma ordem estabelecida no planejamento. As atividades de gerência e de controle da qualidade são, muitas vezes, ditas atividades de apoio, pois não estão ligadas diretamente à construção do produto final: o software a ser entregue para o cliente, incluindo toda a documentação necessária. Essas atividades, normalmente, são realizadas ao longo de todo o ciclo de vida, sempre que necessário ou em pontos preestabelecidos durante o planejamento, ditos marcos ou pontos de controle.

No que concerne às atividades técnicas, tipicamente o processo de software inicia-se com o Levantamento de Requisitos, quando os requisitos do sistema a ser desenvolvido são preliminarmente capturados e organizados. Uma vez capturados, os requisitos devem ser modelados, avaliados e documentados. Uma parte essencial dessa fase é a elaboração de modelos descrevendo o quê o software tem de fazer (e não como fazê-lo), dita Modelagem Conceitual. Até este momento, a ênfase está sobre o domínio do problema e não se deve pensar na solução técnica, computacional a ser adotada~\cite{falboEngReq}.

Com os requisitos pelo menos parcialmente capturados e especificados na forma de modelos, pode-se começar a trabalhar no domínio da solução. Muitas soluções são possíveis para o mesmo conjunto de requisitos e elas são intrinsecamente ligadas a uma dada plataforma de implementação (linguagem de programação, mecanismo de persistência a ser adotado etc.). A fase de projeto tem por objetivo definir e especificar uma solução a ser implementada. É uma fase de tomada de decisão, tendo em vista que muitas soluções são possíveis.

Uma vez projetado o sistema, pode dar-se início à implementação, quando as unidades de software do projeto são implementadas e testadas individualmente. Gradativamente, os elementos vão sendo integrados e testados (teste de integração), até se obter o sistema, quando o todo deve ser testado (teste de sistema). Por fim, uma vez testado no ambiente de desenvolvimento, o software pode ser colocado em produção. Usuários devem ser treinados, o ambiente de produção deve ser configurado e o sistema deve ser instalado e testado, agora pelos usuários no ambiente de produção (testes de homologação ou aceitação). Caso o software demonstre prover as capacidades requeridas, ele pode ser aceito e a operação iniciada.

A Engenharia de Requisitos é o processo pelo qual os requisitos de um produto de software são coletados, analisados, documentados e gerenciados ao longo de todo o ciclo de vida do software~\cite{aurumEngReq}.

Requisitos têm um papel central no desenvolvimento de software, uma vez que uma das principais medidas do sucesso de um software é o grau no qual ele atende aos objetivos e requisitos para os quais foi construído. Requisitos são a base para estimativas, modelagem, projeto, implementação, testes e até mesmo para a manutenção. Portanto, estão presentes ao longo de todo o ciclo de vida de um software~\cite{falboEngReq}.

Sistemas de software são reconhecidamente importantes ativos estratégicos para diversas organizações. Uma vez que tais sistemas, em especial os sistemas de informação, têm um papel vital no apoio aos processos de negócio das organizações, é fundamental que os sistemas funcionem de acordo com os requisitos estabelecidos. Neste contexto, uma importante tarefa no desenvolvimento de software é a identificação e o entendimento dos requisitos dos negócios que os sistemas vão apoiar~\cite{aurumEngReq}. 

Nos estágios iniciais de um projeto, requisitos têm de ser levantados, entendidos e documentados (atividades de Levantamento, Análise e Documentação de Requisitos). Dada a importância dos requisitos para o sucesso de um projeto, atividades de controle da qualidade
devem ser realizadas para verificar, validar e garantir a qualidade dos requisitos, uma vez que os custos serão bem maiores se defeitos em requisitos forem identificados tardiamente. Mesmo quando coletados de forma sistemática, requisitos mudam. Os negócios são dinâmicos
e não há como garantir que os requisitos não sofrerão alterações. Assim, é fundamental gerenciar a evolução dos requisitos, bem como manter a rastreabilidade entre os requisitos e os demais artefatos produzidos no projeto (atividade de Gerência de Requisitos)~\cite{falboEngReq}.

Como se pode observar, o tratamento de requisitos envolve atividades de desenvolvimento (Levantamento, Análise e Documentação de Requisitos), gerência (Gerência de Requisitos) e controle da qualidade (Verificação, Validação e Garantia da Qualidade de Requisitos). Ao conjunto de atividades relacionadas a requisitos, dá-se o nome de Processo de Engenharia de Requisitos.

Neste projeto foram utilizadas as técnicas de levantamento e especificação de requisitos aprendidas ao longo do curso, como descrição de minimundo, levantamento de requisitos funcionais e não-funcionais, modelagem de casos de uso e modelagem conceitual estrutural. Nos parágrafos que se seguem, descrevemos brevemente estas técnicas.

A descrição do minimundo apresenta, em um texto corrido, uma visão geral do domínio do problema a ser resolvido e dos processos de negócio apoiados, bem como as principais ideias do cliente sobre o sistema a ser desenvolvido.

Já os requisitos funcionais, são declarações de serviços que o sistema deve prover, descrevendo o que o sistema deve fazer~\cite{sommerville}. Um requisito funcional descreve uma interação entre o sistema e o seu ambiente~\cite{pfleeger}, podendo descrever, ainda, como o sistema deve reagir a entradas específicas, como o sistema deve se comportar em situações específicas e o que o sistema não deve fazer~\cite{sommerville}.

De forma simplificada, os requisitos funcionais são aqueles que fazem parte do sistema, como um relatório específico, um campo em um cadastro, etc. Eles normalmente têm a finalidade de agregar valor ao usuário ou facilitar o trabalho que ele desenvolve. Requisitos funcionais serão implementados no próprio sistema e a implementação desses requisitos caracteriza um sistema. Os requisitos funcionais identificados para o sistema SAP são descritos no Apêndice ao final dessa monografia.

Assim como os requisitos funcionais precisam ser especificados em detalhes, o mesmo acontece com os requisitos não funcionais. Para os atributos de qualidade considerados prioritários, o analista deve trabalhar no sentido de especificá-los de modo que eles se tornem mensuráveis e, por conseguinte, testáveis. Eles descrevem restrições sobre os serviços ou funções oferecidos pelo sistema~\cite{sommerville}, as quais limitam as opções para criar uma solução para o problema~\cite{pfleeger}.

O modelo de casos de uso é um modelo comportamental, mostrando as funções do sistema, mas de maneira estática. Ele é composto de dois tipos principais de artefatos: os diagramas de casos de uso e as descrições de casos de uso. Um diagrama de casos de uso é um diagrama bastante simples, que descreve o sistema, seu ambiente e como sistema e ambiente estão relacionados. Assim, ele descreve o sistema segundo uma perspectiva externa. As descrições dos casos de uso descrevem o passo a passo para a realização dos casos de uso e são essencialmente textuais~\cite{falboEngReq}. 

Tomando por base casos de uso e suas descrições, é possível passar à modelagem conceitual estrutural, quando os conceitos e relacionamentos envolvidos no domínio são capturados em um conjunto de diagramas de classes. Neste momento é importante definir, também, o significado dos conceitos e de suas propriedades, bem como restrições sobre eles. Essas definições são documentadas em um dicionário de dados do projeto.

Um diagrama de classes exibe um conjunto de classes e seus relacionamentos. Diagramas de classes proveem uma visão estática da estrutura de um sistema e, portanto, são usados na modelagem conceitual estrutural. Para tornar os modelos conceituais mais simples, de modo a facilitar a comunicação com clientes e usuários, tipos de dados de atributos podem ser omitidos do diagrama de classes. Restrições de integridade são regras de negócio e poderiam ser lançadas no Documento de Requisitos. Contudo, como elas são importantes para a compreensão e eliminação de ambiguidades do modelo conceitual, é útil descrevê-las no próprio modelo conceitual.~\cite{falboEngReq}. 

%%% Início de seção. %%%
\section{Conceitos de Java}
\label{sec-referencial-java}

Durante muitos anos, os usuários se habituaram com aplicações Desktop. Este tipo de aplicação é instalada no computador local e acessa diretamente um banco de dados ou gerenciador de arquivos. Entretanto, existem algumas desvantagens no desenvolvimento Desktop. Como cada usuário tem uma cópia integral da aplicação, qualquer alteração precisaria ser propagada para todas as outras máquinas. Estamos usando um cliente gordo, isto é, com muita responsabilidade no lado do cliente. Note que, aqui, estamos chamando de cliente a aplicação que está rodando na máquina do usuário. Para piorar, as regras de negócio rodam no computador do usuário. Isso faz com que seja muito mais difícil depurar a aplicação, já que não costumamos ter acesso tão fácil à maquina onde a aplicação está instalada. Em geral, enfrentamos problemas de manutenção e gerenciamento~\cite{caelumjava}.

Para resolver problemas como esse, surgiram as aplicações baseadas na Web. Nessa abordagem há um servidor central onde a aplicação é executada e processada e todos os usuários podem acessá-la através de um cliente simples e do protocolo HTTP\footnote{HTTP -- https://pt.wikipedia.org/wiki/Hypertext\_Transfer\_Protocol} (\textit{HyperText Transfer Protocol, em português ``Protocolo de Transferência de Hipertexto''}). Um navegador Web, como Firefox\footnote{Firefox -- https://www.mozilla.org/pt-BR/firefox/} ou Chrome\footnote{Chrome -- https://www.google.com/chrome/}, que fará o papel da aplicação cliente, interpretando HTML\footnote{HTML -- https://pt.wikipedia.org/wiki/HTML} (\textit{Hypertext Markup Language, em português ``Linguagem de Marcação de Hipertexto''}), CSS(\textit{Cascading Style Sheets, em português ``folhas de estilo em cascata''}) e JavaScript - que são as tecnologias que ele entende.

Enquanto o usuário usa o sistema, o navegador envia requisições (\textit{requests}) para o lado do servidor (\textit{server side}), que responde para o computador do cliente (\textit{client side}). Em nenhum momento a aplicação está salva no cliente: todas as regras da aplicação estão no lado do servidor. Por isso, essa abordagem também foi chamada de cliente magro (\textit{thin client}). A Figura~\ref{fig-referencial-aplicacao-web} exibe a arquitetura Web mencionada~\cite{caelumjava}.

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{figuras/referencial/fig-referencial-aplicacao-web}
	\caption{Arquitetura Aplicação Web~\cite{caelumjava}.}
	\label{fig-referencial-aplicacao-web}
\end{figure}

Isso facilita bastante a manutenção e a gerenciabilidade, pois temos um lugar central e acessível onde a aplicação é executada. Contudo, note que será preciso conhecer HTML, CSS e JavaScript, para fazer a interface com o usuário, e o protocolo HTTP para entender a comunicação pela Web. E, mais importante ainda, não há mais eventos, mas sim um modelo bem diferente orientado a requisições e respostas. Toda essa base precisará ser conhecida pelo desenvolvedor.

Comparando as duas abordagens, podemos ver vantagens e desvantagens em ambas. No lado da aplicação puramente Desktop, temos um estilo de desenvolvimento orientado a eventos, usando componentes ricos, porém com problemas de manutenção e gerenciamento. Do outro lado, as aplicações Web são mais fáceis de gerenciar e manter, mas precisamos lidar com HTML, conhecer o protocolo HTTP e seguir o modelo requisição/resposta~\cite{caelumjava}.

Em vez de desenvolver puramente para Desktop, é uma tendência mesclar os dois estilos, aproveitando as vantagens de cada um. Em outras palavras, realizar um desenvolvimento Desktop para a Web, tanto central quanto com componentes ricos, aproveitando o melhor dos dois mundos e abstraindo o protocolo de comunicação. Essa é justamente a ideia dos frameworks Web baseados em componentes. No mundo Java há algumas opções como JavaServer Faces (JSF)\footnote{JavaServer Faces (JSF) -- https://javaserverfaces.java.net/}, Apache Wicket\footnote{Apache Wicket -- http://wicket.apache.org/}, Vaadin\footnote{Vaadin -- https://vaadin.com/home}, Tapestry\footnote{Tapestry -- http://tapestry.apache.org/} ou GWT da Google\footnote{GWT -- http://www.gwtproject.org/}. Todos eles são frameworks Web baseados em componentes.

JSF, parte da plataforma Java EE (\textit{Enterprise Edition}), é uma tecnologia que nos permite criar aplicações Java para Web utilizando componentes visuais pré-prontos, de forma que o desenvolvedor não se preocupe com Javascript e HTML. Basta adicionarmos os componentes (calendários, tabelas, formulários) e eles serão renderizados e exibidos em formato HTML~\cite{caelumjava}.

Além disso o estado dos componentes é sempre guardado automaticamente (como veremos mais à frente), criando a característica \textit{Stateful}. Isso nos permite, por exemplo, criar formulários de várias páginas e navegar nos vários passos dele com o estado das telas sendo mantidos. Outra característica marcante na arquitetura do JSF é a separação que fazemos entre as camadas de apresentação e de aplicação. Pensando no modelo MVC (\textit{Model-View-Controller})\footnote{MVC -- https://pt.wikipedia.org/wiki/MVC}, o JSF possui uma camada de visualização bem separada do conjunto de classes de modelo. O JSF ainda tem a vantagem de ser uma especificação do Java EE, isto é, todo servidor de aplicações Java tem que vir com uma implementação dela e há diversas outras disponíveis.

Existem diversas extensões baseadas na tecnologia JavaServer Faces que se destinam a tornar mais simples o uso de AJAX (\textit{Asynchronous Javascript and XML, em português ``Javascript e XML Assíncronos''}) e componentes em aplicações Web. Este é o caso do PrimeFaces que é uma biblioteca de componentes de código aberto para o JSF 2.0 com mais de 100 componentes, permitindo criar interfaces ricas para aplicações Web de forma simplificada e eficiente. Ele possui as seguintes vantagens em relação a outras bibliotecas de componentes JSF, tendo em vista que~\cite{caelumjava}:

\begin{itemize}
	\item Possui um rico conjunto de componentes de interface (DataTable, AutoComplete, HTMLEditor, gráficos etc);	
	\item Nenhum XML de configuração extra é necessário e não há dependências;
	\item Componentes construídos com AJAX no padrão JSF 2.0 AJAX APIs;	
	\item Mais de 25 temas templates;
	\item Boa documentação com exemplos de código;
	\item Não possui nenhuma outra dependência, ou seja, basta colocar a biblioteca do PrimeFaces em seu projeto e começar a utilizar, não existindo nenhum \textit{overhead} (geralmente considerado como qualquer processamento ou armazenamento em excesso, seja de tempo de computação, de memória ou qualquer outro recurso que seja requerido para ser utilizado ou gasto para executar uma determinada tarefa.).
\end{itemize}

O PrimeFaces é um framework da Prime Teknoloji (empresa da Turquia) que oferece um conjunto de componentes ricos para o JavaServer Faces. Seus componentes foram construídos para trabalhar com AJAX por ``default'', isto é, não é necessário nenhum esforço extra por parte do desenvolvedor para realização de chamadas assíncronas ao servidor. Além disso, o PrimeFaces permite a aplicação de temas (\textit{skins}) com o objetivo de mudar a aparência dos componentes de forma simples.

Além do JSF, outro componente da plataforma Java EE utilizado foi o CDI. O CDI (Contexts and Dependency Injection for the Java\texttrademark EE Platform, em português ``Contextos e Injeção de Dependências para a plataforma Java\texttrademark EE'') foi proposto pela JSR 299 (\textit{Java Specification Requests})\footnote{JSR -- https://jcp.org/en/jsr/detail?id=299}, cuja versão final aprovada pelo JCP (\textit{Java Community Process})\footnote{JCP -- https://www.jcp.org/en/home/index} foi publicada em dezembro de 2009 e incluída na plataforma Java EE 6 para desenvolvimento de aplicações corporativas. Permite que declaremos uma dependência de uma classe do sistema (chamada de bean) a um EJB (\textit{Enterprise JavaBeans})\footnote{EJB -- https://pt.wikipedia.org/wiki/Enterprise\_JavaBeans} utilizando a anotação \texttt{@EJB} ou a uma classe não-EJB utilizando \texttt{@Inject}, ambos sobre o atributo que representa a associação entre o dependente e sua dependência. Provê acesso via linguagem de expressões unificada (\textit{Expression Language}, ou EL) a beans que utilizarem a anotação \texttt{@Named} na definição da classe. Tal anotação permite definir um nome para o componente ou utilizar o nome default: o mesmo nome da classe, trocando a primeira letra para minúscula. Traz consigo alguns estereótipos que representam papéis desempenhados por determinados componentes em arquiteturas comuns e padrões de projeto, como o \texttt{@Model}, que representa o modelo em uma arquitetura MVC. Possibilita, ainda, que o desenvolvedor crie seus próprios estereótipos. As classes gerenciadas pelo CDI são associadas a determinados contextos para gerenciamento automático do seu ciclo de vida. O CDI oferece, além disso, uma série de funcionalidades como qualificadores, alternativas, decoradores, interceptadores e eventos que permitem uma grande flexibilidade no desenvolvimento da aplicação~\cite{devmediacdi}.

Já os objetos de acesso a dados (ou simplesmente DAO, acrônimo de \textit{Data Access Object}), são um padrão para persistência de dados que permitem separar regras de negócio das regras de acesso a banco de dados. Numa aplicação que utilize a arquitetura MVC, todas as funcionalidades de bancos de dados, tais como obter as conexões, mapear objetos Java para tipos de dados SQL (\textit{Structured Query Language, em português ``Linguagem de Consulta Estruturada''}) ou executar comandos SQL, devem ser feitas por classes DAO.

A vantagem de usar objetos de acesso a dados é a separação simples e rigorosa entre duas partes importantes de uma aplicação que não devem e não podem conhecer quase que nada uma da outra, e que podem evoluir frequentemente e independentemente. Alterar a lógica de negócio pode esperar apenas a implementação de uma interface, enquanto que modificações na lógica de persistência não alteram a lógica de negócio, desde que a interface entre elas não seja modificada. Segue abaixo mais algumas vantagens:

\begin{itemize}
	\item Pode ser usada em uma vasta porcentagem de aplicações;	
	\item Esconde todos os detalhes relativos a armazenamento de dados do resto da aplicação;	
	\item Atua como um intermediário entre a aplicação e o banco de dados;	
	\item Mitiga ou resolve problemas de comunicação entre a base de dados e a aplicação, evitando estados inconsistentes de dados.
\end{itemize}

No contexto específico da linguagem de programação Java, um objeto de acesso a dados como padrão de projeto de software pode ser implementado de várias maneiras. Pode variar desde uma simples interface que separa partes de acesso a dados da lógica de negócio de uma aplicação até frameworks e produtos comerciais específicos.

Após diversos anos de reclamações sobre a complexidade na construção de aplicações com Java, a especificação Java EE 5 teve como principal objetivo a facilidade para desenvolver aplicações corporativas. O EJB 3 foi o grande percursor para essa mudança fazendo os Enterprise JavaBeans mais fáceis e mais produtivos de usar~\cite{devmediajpa}.

No caso dos Session Beans e Message-Driven Beans, a solução para questões de usabilidade foram alcançadas simplesmente removendo alguns dos mais onerosos requisitos de implementação e permitindo que os componentes sejam como POJOS (\textit{Plain Old Java Object ou Velho e Simples Objeto Java}.

Já os Entity Beans eram um problema muito mais sério. A solução foi começar do zero. Deixou-se os Entity Beans sozinhos e introduziu-se um novo modelo de persistência. A versão atual da JPA (\textit{Java Persistence API}) nasceu através das necessidades dos profissionais da área e das soluções proprietárias que já existiam para resolver os problemas com persistência. Com a ajuda dos desenvolvedores e de profissionais experientes que criaram outras ferramentas de persistência, chegou a uma versão muito melhor que é a que os desenvolvedores Java conhecem atualmente.

Dessa forma os líderes das soluções de mapeamento objetos-relacionais deram um passo adiante e padronizaram também os seus produtos. Hibernate\footnote{Hibernate -- http://hibernate.org/} e TopLink\footnote{TopLink -- http://www.oracle.com/technetwork/middleware/toplink/overview/index.html} foram os primeiros a firmar com os fornecedores EJB.

O resultado final da especificação EJB finalizou com três documentos separados, sendo que o terceiro era o JPA. Essa especificação descrevia o modelo de persistência em ambos os ambientes Java SE (\textit{Standard Edition})\footnote{Java SE -- http://www.oracle.com/technetwork/pt/java/javase/overview/index.html} e Java EE.

No momento em que a primeira versão do JPA foi iniciada, outros modelos de persistência ORM (\textit{Object Relational Mapping, em português ``Mapeamento Objeto Relacional''}) já haviam evoluído. Mesmo assim muitas características foram adicionadas nesta versão e outras foram deixadas para uma próxima versão.

A versão JPA 2.0 incluiu um grande número de características que não estavam na primeira versão, especialmente as mais requisitadas pelos usuários, entre elas a capacidade adicional de mapeamento, expansões para a Java Persistence Query Language (JPQL), a API Criteria para criação de consultas dinâmicas, entre outras características.

Entre as principais inclusões na JPA destacam-se~\cite{devmediajpa}:

\begin{itemize}
	\item \textbf{POJOS Persistentes:} Talvez o aspecto mais importante da JPA seja o fato que os objetos são POJOs, significando que os objetos possuem design simples que não dependem da herança de interfaces ou classes de frameworks externos. Qualquer objeto com um construtor default pode ser feito persistente sem nenhuma alteração numa linha de código. Mapeamento Objeto-Relacional com JPA é inteiramente dirigido a metadados. Isto pode ser feito através de anotações no código ou através de um XML definido externamente;
	
	\item \textbf{Consultas em Objetos:} As consultas podem ser realizadas através da Java Persistence Query Language (JPQL), uma linguagem de consulta que é derivada do EJB QL e transformada depois para SQL. As consultas usam um esquema abstraído que é baseado no modelo de entidade como oposto às colunas na qual a entidade é armazenada;
	
	\item \textbf{Configurações simples:} Existe um grande número de características de persistência que a especificação oferece, todas são configuráveis através de anotações, XML ou uma combinação das duas. Anotações são simples de usar, convenientes para escrever e fácil de ler. Além disso, JPA oferece diversos valores defaults, portanto para já sair usando JPA é simples, bastando algumas anotações;
	
	\item \textbf{Integração e Teste:} Atualmente as aplicações normalmente rodam num Servidor de aplicação, sendo um padrão do mercado hoje. Testes em servidores de aplicação são um grande desafio e normalmente impraticáveis. Efetuar teste de unidade e teste caixa branca em servidores de aplicação não é uma tarefa tão trivial. Porém, isto é resolvido com uma API que trabalha fora do servidor de aplicação. Isto permite que a JPA possa ser utilizada sem a existência de um servidor de aplicação. Dessa forma, testes unitários podem ser executados mais facilmente.
\end{itemize}

O Hibernate provê três tipos diferentes para retornar informações, uma é a HQL, outra forma é através de consultas nativas e a terceira forma é através da API Criteria~\cite{devmediacri}.

A API Criteria nos permite construir consultas estruturadas utilizando Java, e ainda provê uma checagem de sintaxe em tempo de compilação que não é possível com uma linguagem de consulta como HQL (\textit{Hibernate Query Language}) ou SQL. A API Criteria também fornece projeção, agregação e outras funcionalidades. Possui a interface \texttt{org.hibernate.Criteria} que está disponível com todos os métodos fornecidas pela API. Através dessa API podemos construir consultas de forma programática. A interface \texttt{Session} do Hibernate contém vários métodos \texttt{createCriteria()} que recebem como parâmetro uma classe de um objeto persistente ou o nome da entidade e, dessa forma, o Hibernate irá criar um objeto Criteria que retorna instâncias das classes de objetos persistentes quando a aplicação executa uma consulta com a API.

A API Criteria também permite que possamos criar restrições onde restringimos os objetos retornados, podendo ter mais que uma restrição para uma consulta. Embora possamos criar nossos próprios objetos implementando o objeto \texttt{Criterion} ou estendendo um objeto \texttt{Criterion} já existente, recomenda-se que os desenvolvedores usem esses objetos embutidos na lógica de negócio da aplicação. Também podemos criar nossa própria classe \texttt{Factory} que retorna instâncias do objeto \texttt{Criterion} do Hibernate apropriadamente configurados para as restrições da nossa aplicação.

%%% Início de seção. %%%
\section{FrameWeb}
\label{sec-referencial-frameweb}

FrameWeb (\textit{Framework-based Design Method for Web Engineering}) é baseada em metodologias e linguagens de modelagens bastante difundidas
na área de Engenharia de Software sem, no entanto, impor nenhum processo de desenvolvimento específico. É esperado, apenas, que determinadas atividades comuns à maioria dos processos de software, como levantamento de requisitos, análise, projeto, codificação, testes e implantação, sejam conduzidas pela equipe de desenvolvimento~\cite{vitorFrameWeb}.

O método, portanto, sugere um processo de software orientado a objetos contendo as fases descritas anteriormente, mas podendo ser estendido e adaptado pela equipe de desenvolvimento. A linguagem de modelagem UML (\textit{Unified Modeling Language})~\cite{boochUML} é utilizada durante todo o processo. Podemos dizer também que é um método de projeto para construção de sistemas de informação Web (\textit{Web Information Systems – WISs}) baseado em \textit{frameworks}. Abaixo apresentamos algumas motivações principais desse método:

\begin{itemize}
	\item O uso de \textit{frameworks} ou arquiteturas baseadas em \textit{containers} similares a eles se tornou o padrão de fato para o desenvolvimento de aplicações distribuídas, em especial os baseados na plataforma Web;
	
	\item O uso de métodos que se adequam diretamente à arquitetura de software adotada promove uma agilidade maior ao processo, característica que é bastante desejada na maioria dos projetos Web~\cite{presmannSoft}.
\end{itemize}

Em linhas gerais, FrameWeb assume que determinados tipos de \textit{frameworks} serão utilizados durante a construção da aplicação, define uma arquitetura básica para o WIS e propõe modelos de projeto que se aproximam da implementação do sistema usando esses \textit{frameworks}.

Sendo um método para a fase de Projeto, não prescreve um processo de software completo. No entanto, sugere o uso de um processo de desenvolvimento que contemple as fases apresentadas na Figura~\ref{fig-referencial-processo-software}. Para uma melhor utilização de FrameWeb, espera-se que sejam construídos diagramas de casos de uso e de classes de domínio (ou similares) durante as fases de Requisitos e Análise. Além disso, como já mencionado anteriormente, agilidade é uma característica desejada num processo de software para a Web e, portanto, sugere-se que princípios de agilidade sejam seguidos, em especial os seguintes propostos pela Modelagem Ágil~\cite{agileModel}:

\begin{itemize}
	\item \textbf{Modele com um propósito:} crie apenas os modelos que adicionam informação útil;
	
	\item \textbf{Viaje com pouca bagagem:} ao passar de uma fase para outra no processo de desenvolvimento, alguns modelos precisarão ser adequados à nova fase. Leve apenas aqueles que serão úteis na fase seguinte;
	
	\item \textbf{Conteúdo é mais importante que apresentação:} utilize a notação e a linguagem mais adequadas ao objetivo principal de um modelo, que é a transmissão da informação;
	
	\item \textbf{Conheça os modelos e as ferramentas:} equipes de modelagem e desenvolvimento devem ser altamente proficientes nas linguagens e ferramentas utilizadas na modelagem;
	
	\item \textbf{Adapte-se localmente:} as equipes devem ser capazes de se adaptar às necessidades específicas de um projeto.
\end{itemize}

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{figuras/referencial/fig-referencial-processo-software}
	\caption{Processo de Desenvolvimento de Software.}
	\label{fig-referencial-processo-software}
\end{figure}

A fase de Projeto concentra as propostas principais do método: (i) definição de uma arquitetura padrão que divide o sistema em camadas, de modo a se integrar bem com os \textit{frameworks} utilizados; (ii) proposta de um conjunto de modelos de projeto que trazem conceitos utilizados pelos \textit{frameworks} para esta fase do processo por meio da criação de um perfil UML que faz com que os diagramas fiquem mais próximos da implementação.

O FrameWeb define extensões leves (\textit{lightweight extensions}) ao meta-modelo da UML para representar componentes típicos da plataforma Web e dos \textit{frameworks} utilizados, criando um perfil UML que é utilizado para a construção de diagramas de quatro tipos:

\begin{itemize}
	\item \textbf{Modelo de Domínio:} é um diagrama de classes da UML que representa os objetos de domínio do problema e seu mapeamento para a persistência em banco de dados relacional;
	
	\item \textbf{Modelo de Aplicação:} é um diagrama de classes da UML que representa as classes de serviço, que são responsáveis pela codificação dos casos de uso, e suas dependências;
		
	\item \textbf{Modelo de Navegação:} é um diagrama de classe da UML que representa os diferentes componentes que formam a camada de Lógica de Apresentação, como páginas Web, formulários HTML e classes controladoras;
	
	\item \textbf{Modelo de Persistência:} é um diagrama de classes da UML que representa as classes DAO existentes, responsáveis pela 	persistência das instâncias das classes de domínio.
\end{itemize}

Esses quatro modelos serão descritos com maiores detalhes na Seção~\ref{sec-projeto-modelos-frame-web}.